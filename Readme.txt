Version 0.3.2 - Last updated 25/10/08 @ 18:50
Contact Details: sniperumm@hotmail.com

Updated with alot of help from Perkus :) Thanks buddy.

Normal Commands

/nisp on - Turns on NISP
/nisp off - Turns off NISP

Debug Commands

/nisp debug on - Turns debugging on
/nisp debug off - Turns debugging off
/nisp debug dumpclear - Clears dumped items

Please Read
Debugging is to be used when you've found an item that has no price and contains "no sell price".

Follow these steps:

    * Put item into backpack
    * Turn on the debugging (/nisp debug on)
    * Hold down shift key + click right mouse button
    * Do the last step for every item then continue
    * Type /reloadui (This will reload all ui elements and also store the data in a file)
    * Now go to the following on your computer: (where ever you installed it)\Warhammer Online - Age of Reckoning\user\interface\AllCharacters\NPC Item Sale Price\
    * Email SavedVariables.lua to nispdebugfiles@sniperumm.co.uk with your name.

These are temporary until I add a simple gui button.