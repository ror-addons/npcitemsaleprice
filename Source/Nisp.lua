-- Perkus:  Oct-23-08  Fixed the following:
-- 1) Searched & Replaced ChatWindow -> EA_ChatWindow (SlashHandler was totally broken since WAR's latest UI change)
-- 2) Fixed Nisp.SetItemTooltipData:
--	a) For equippable items it was reporting price of last comparisontooltips instead of equipped item
--	b) Repair item handling was also wrong - after you got a tooltip for one, it would suppress the tooltip on the next regular item, etc.
--	c) It now correctly shows prices of all comparison items a well as broken items in addition to the current item!
-- 3) Added a "Nisp initialized message" on Init()
-- 5) Added debug statements to dump tooltip names and prices when debug mode is on
--
--TO DO:
-- b) There's a really minor bug with "No sell price" items like trophies and pockets: they only show the "No Sell Price" string once when you switch
--	to their tooltip from a tooltip that *does* have a price.  Thereafter the tooltip won't show this string upon repeated display.
--	I don't know how to make it do that properly, and in fact it's mostly likely Mythic's bug and not fixable via a mod unless you hack it somehow.
--	Odds are it's exactly the same way it behaves with 0 mods running...


Nisp = {}

local OldSetItemTooltipData = nil
local OldInventoryRButtonUp = nil
local MouseOverItemData = nil
local prevTooltipWindow = nil;

function Nisp.Init()

	OldSetItemTooltipData = Tooltips.SetItemTooltipData
	OldInventoryRButtonUp = EA_Window_Backpack.InventoryRButtonUp
	EA_Window_Backpack.InventoryRButtonUp = Nisp.RButtonUp

	LibSlash.RegisterSlashCmd("nisp", function(args) Nisp.SlashHandler(args) end)

	Tooltips.SetItemTooltipData = Nisp.SetItemTooltipData

	if Nisp.Enabled == nil then
		Nisp.Enabled = true
		EA_ChatWindow.Print(L"Nisp Installed and Enabled")
	elseif Nisp.Enabled == true then
		EA_ChatWindow.Print(L"Nisp Initialized and Enabled (/nisp for commands)")
	elseif Nisp.Enabled == false then
		EA_ChatWindow.Print(L"Nisp Initialized, but disabled (/nisp for commands)")
	end
	if Nisp.DebugEnabled == nil then Nisp.DebugEnabled = false end
	if Nisp.DumpItemsTable == nil then Nisp.DumpItemsTable = {} end
end


function Nisp.SlashHandler(args)
	local opt, val = args:match("([a-z0-9]+)[ ]?(.*)")
	if not opt then
		EA_ChatWindow.Print(L"Nisp commands:")
		EA_ChatWindow.Print(L"/nisp on - to turn on npc item sales prices")
		EA_ChatWindow.Print(L"/nisp off - to turn off npc item sales prices")
		EA_ChatWindow.Print(L"Debug Commands:")
		EA_ChatWindow.Print(L"/nisp debug on - to turn debugging on")
		EA_ChatWindow.Print(L"/nisp debug off - to turn debugging off")
		EA_ChatWindow.Print(L"/nisp debug dumpclear - to clear dumped items")
	elseif opt == "on" then
		Nisp.Enabled = true
		EA_ChatWindow.Print(L"Nisp Enabled")
	elseif opt == "off" then
		Nisp.Enabled = false
		EA_ChatWindow.Print(L"Nisp Disabled")
	elseif opt == "debug" then
		if val == "on" then Nisp.DebugEnabled = true; EA_ChatWindow.Print(L"Nisp debugging on (hold down SHIFT)")	--ARM added "hold down Shift"
		elseif val == "off" then Nisp.DebugEnabled = false; EA_ChatWindow.Print(L"Nisp debugging off")
		elseif val == "dumpclear" then Nisp.DumpClear() end
	end
end


-------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Perkus: NOTE: This function gets called at least TWICE when mousing over equippable items (i.e. weapons, armor, accessories, etc.) if the player has an
-- item equipped in the matching slot!  First call is for the item itself (with a tooltipWindow == "ItemTooltip"), and the second call is for the currently
-- currently equipped item (with a tooltipWindow == "ItemComparisonTooltip").  Actually, if you're running the mod "preciousss", if gets called up to 4
-- additional times when mouse over a jewelry item - once for every equipped jewelry item!
-- Exception: Tooltips over repairable items call with tooltipWindow == BrokenItemTooltipRepairedItem instead of "ItemTooltip", and may also call
-- with additional Comparison tooltips!
-- In order to show the correct itemData for each tooltip item, you must set Tooltips.curTooltipWindow = tooltipWindow, then restore it so it gets clean up!
-------------------------------------------------------------------------------------------------------------------------------------------------------------
function Nisp.SetItemTooltipData( tooltipWindow, itemData, extraText, extraTextColor )
	OldSetItemTooltipData( tooltipWindow, itemData, extraText, extraTextColor );

	local strToolTipWindowName = tostring(tooltipWindow)

	if Nisp.DebugEnabled then
		EA_ChatWindow.Print(L"tooltipWindow = " .. towstring(strToolTipWindowName))
	end

	-- Quick fix for Book Of Binding.
	if itemData.uniqueID == 11919 or itemData.name == L"Book of Binding" then
		return
	end

	if Nisp.Enabled then
		if Nisp.DebugEnabled then
			EA_ChatWindow.Print(L"Showing Sell Price = " .. towstring(itemData.sellPrice))
		end

		-- save the current tooltip window
		local prevTooltipWindow = Tooltips.curTooltipWindow
		-- make the tooltip window for which we're being called the "current" tooltip window
		Tooltips.curTooltipWindow = strToolTipWindowName
		-- add the sell price to that window
		Tooltips.ShowSellPrice(itemData);
		-- restore the current tooltip window (required for proper clean-up when multiple tooltips are shown at once)
		Tooltips.curTooltipWindow = prevTooltipWindow
	end

-- Perkus: v3.1.2:
--	-- Had a problem with BrokenItemWindow sticking due to forcing ItemTooltip onto Tooltips.curTooltipWindow.
--	-- Solved this by setting Tooltips.curTooltipWindow to "ItemTooltip" temporary then setting it back to what it was.
--	if Enabled then
--		currWindow = Tooltips.curTooltipWindow
--		lastwindow = nil
--
--		if currWindow == "ItemTooltip" or itemData ~= nil then
--			lastwindow = currWindow
--			Tooltips.curTooltipWindow = "ItemTooltip"
--			Tooltips.ShowSellPrice(itemData);
--			Tooltips.curTooltipWindow = currWindow
--		end
--	end
end



------------------------ DEBUG TOOLS ---------------------------

function Nisp.RButtonUp(buttonId, flags)
	if(flags == SystemData.ButtonFlags.SHIFT and Nisp.DebugEnabled) then
		slot = EA_Window_Backpack.GetSlotFromActionButtonGroup( SystemData.ActiveWindow.name, buttonId )
		MouseOverItemData = DataUtils.GetItems()[slot]
		Nisp.DumpItem(MouseOverItemData.name)
	else
		OldInventoryRButtonUp(buttonId, flags)
	end
end

function Nisp.DumpItem(itemName)
	itemData = Nisp.GetItemByName(itemName)
	if(Nisp.Contains(itemData.uniqueID) == false) then
		table.insert(Nisp.DumpItemsTable, itemData)
		Nisp.Contains(itemData.name)
		EA_ChatWindow.Print(L"Dumped item: "..itemData.name)
	else
		EA_ChatWindow.Print(L"Item Dump contains item: "..itemData.name)
	end
end

function Nisp.GetItemByName(itemName)
	for slotId, itemData in pairs(DataUtils.GetItems()) do
		if itemData.name == itemName and itemName ~= "" then
			return itemData
		end
	end
end

function Nisp.Contains(uniqueID)
	for slotId, itemData in ipairs(Nisp.DumpItemsTable) do
		if(itemData.uniqueID == uniqueID) then
			return true
		end
	end

	return false
end

function Nisp.DumpClear()
	Nisp.DumpItemsTable = {}
	EA_ChatWindow.Print(L"Dump Table Cleared")
end
